This repo has two branches :
1. backend - It contains the code for the backend warehouse application with an MDB and websocket
2. ECommerce-Frontend - It contains the code for the frontend app that lets us create order to be sent to backend

Technologies Used:
1. J2EE - JSF, EJB, CDI, JMS, Websocket
2. AngularJS