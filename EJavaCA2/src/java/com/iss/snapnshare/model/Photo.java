package com.iss.snapnshare.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Gautam
 */
@Entity
@Table(name = "photos")
public class Photo {
    
    private static final long serialVersionUID = 1L;

    @Id @Column(name="photoId")
    private String id;

    @Basic
    @Column(name="photoUrl")
    private String photoUrl;

    @Column(name="comment")
    private String comment;

    @Column(name="postTime")
    private String postTime;

    @Column(name="UserId")
    private String postedBy;
        
        

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public String getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }
                        
    public JsonObjectBuilder getPhotoJsonbuilder() {

            //Builder
            return (Json.createObjectBuilder()
                            .add("postedBy", postedBy)
                            .add("url", photoUrl)
                            .add("comment", comment)
                            .add("postTime", postTime));
                            //.build());
    }
}
