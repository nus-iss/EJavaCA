/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iss.snapnshare.rest;

import com.iss.snapnshare.business.TimelineBean;
import com.iss.snapnshare.business.UserBean;
import com.iss.snapnshare.model.Photo;
import com.iss.snapnshare.model.User;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Gautam
 */

@RequestScoped
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON) 
@Path("/")
public class SnsResource {
    
    //@EJB private User user;
    @EJB private UserBean userBean;
    @EJB private TimelineBean timelineBean;
    
    @POST
    @Path("/register/{username}")
    public Response register(@PathParam("username") String username){
        
        if(username!=null) {
        
        User user = new User();
        user.setId(username);
        user.setName(username);
        
        userBean.addUser(user);
        
        JsonObject result = Json.createObjectBuilder()
                .add("registerId", username)
                .build();
        return (Response
                    .status(Response.Status.CREATED) //setStatus
                    .entity(result) //PrintWriter
                    .build());
        }
        return (Response
                            .status(Response.Status.FORBIDDEN) //setStatus
                            //.entity(result) //PrintWriter
                            .build());
    }
    
    @POST
    @Path("/friends/{username}")
    public Response addFriend(@PathParam("username") String username, String json){
        if(username==null) return null;
        User u = userBean.getUserByName(username);
        String tmp = json.replaceAll("\"", "");
        String tmp2 = tmp.replaceAll("\\[", "");
        String tmp3 = tmp2.replaceAll("\\]", "");
        System.out.println(tmp3);
        u.setFriends(tmp3);
//        if(u!=null && u.getFriends()!=null) {
//            u.setFriends(u.getFriends() +","+ username);
//        }else{
//            u.setFriends(username);
//        }
        userBean.addFriend(u);
        
        
        JsonObject result = Json.createObjectBuilder()
                .add("registerId", username)
                .build();
        return (Response
                            .status(Response.Status.CREATED) //setStatus
                            .entity(result) //PrintWriter
                            .build());
    }
    
//    @GET
//    @Path("/register")
//    public Response getRegister(){
//        
//        JsonObject result = Json.createObjectBuilder()
//                .add("registerId", "FERJI")
//                .build();
//        return (Response
//                            .status(Response.Status.CREATED) //setStatus
//                            .entity(result) //PrintWriter
//                            .build());
//    }
    
//    @GET
//    @Path("/friends")
//    public Response getAllFriends(){
//        List<User> friends =  userBean.getAllFriends();
//        
//        JsonArrayBuilder bldr  = Json.createArrayBuilder();
//        for(User friend:friends){
//            bldr.add(friend.getName());
//        }
//        
//        JsonArray result = bldr.build();
//        return (Response
//                            .status(Response.Status.CREATED) //setStatus
//                            .entity(result) //PrintWriter
//                            .build());
//    }
    
    @GET
    @Path("/friends/{username}")
    public Response getMyFriends(@PathParam("username") String username){
        List<User> friends =  userBean.getMyFriends(username);
        if(friends!=null){
        JsonArrayBuilder bldr  = Json.createArrayBuilder();
            for(User friend:friends){
                if(friend.getFriends()!=null){
                    bldr.add(friend.getFriends());
                }
            }

            JsonArray result = bldr.build();
            return (Response
                                .status(Response.Status.CREATED) //setStatus
                                .entity(result) //PrintWriter
                                .build());
        }
        return(Response
                            .status(Response.Status.ACCEPTED) //setStatus
                           // .entity(result) //PrintWriter
                            .build());
    }
    
    @GET
    @Path("/timeline/{username}")
    public Response getTimelineData(@PathParam("username") String username){
        List<User> friends = userBean.getMyFriends(username);
        if(friends!=null && friends.get(0)!=null && friends.get(0).getFriends()!=null){
            List<String> friendList = Arrays.asList(friends.get(0).getFriends().split(","));
            List<Photo> photos =  timelineBean.getTimelineData(friendList);

            JsonArrayBuilder bldr  = Json.createArrayBuilder();
            for(Photo photo:photos){
                bldr.add(photo.getPhotoJsonbuilder());
            }
            JsonArray result = bldr.build();

            return (Response
                                .status(Response.Status.ACCEPTED) //setStatus
                                .entity(result) //PrintWriter
                                .build());
        }
        return(Response
                            .status(Response.Status.ACCEPTED) //setStatus
                           // .entity(result) //PrintWriter
                            .build());
    }
    
    
    
}
