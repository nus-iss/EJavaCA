package com.iss.snapnshare.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Gautam
 */

@ApplicationPath("/api")
public class AppConfig extends Application { }
