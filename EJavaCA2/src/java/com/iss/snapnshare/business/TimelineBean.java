/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iss.snapnshare.business;

import com.iss.snapnshare.model.Photo;
import com.iss.snapnshare.model.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Gautam
 */
@Stateless
public class TimelineBean {
    
        @PersistenceContext private EntityManager em;
 
        public List<Photo> getTimelineData(List<String> friendList) {

		TypedQuery<Photo> query = em.createQuery("select p from Photo p where p.postedBy IN :friendList", Photo.class);
                query.setParameter("friendList", friendList);
		return (query.getResultList());
	}
    
}
