/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iss.snapnshare.business;

import com.iss.snapnshare.model.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Gautam
 */

@Stateless
public class UserBean {
    
    	@PersistenceContext private EntityManager em;

	public List<User> getAllFriends() {

		TypedQuery<User> query = em.createQuery("select u from User u", User.class);
		return (query.getResultList());
	}
        
        public List<User> getMyFriends(String username) {

		TypedQuery<User> query = em.createQuery("select u from User u where u.name = :username", User.class);
                query.setParameter("username", username);
		return (query.getResultList());
	}
        
//        public List<User> getTimelineData(String username) {
//
//		TypedQuery<User> query = em.createQuery("select u from User u where u.name = :username", User.class);
//                query.setParameter("username", username);
//		return (query.getResultList());
//	}
        
        public User getUserByName(String username) {
		TypedQuery<User> query = em.createQuery("select u from User u where u.name = :username", User.class);
                query.setParameter("username", username);
		return (query.getSingleResult());
	}
        public void addUser(User user) {
		em.persist(user);
	}
    
        public void addFriend(User user) {
            em.merge(user);
	}
}
